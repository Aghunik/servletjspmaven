<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>home</title>
</head>
<body>

<h1>Welcome </h1>


<h2>All Users</h2>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Surname</td>
        <td>Email</td>

    </tr>
    <c:forEach var="u" items="${users}">

    <tr>
        <td>${u.name}
        </td>
        <td>${u.surname}
        </td>
        <td>${u.email}
        </td>
    </tr>
    </c:forEach>
</body>
</html>
